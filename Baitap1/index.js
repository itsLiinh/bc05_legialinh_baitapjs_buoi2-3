/**BÀI 1
 *
 * input: aDaySalary: lương 1 ngày , workedDay: số ngày làm
 *
 * Step 1: Tạo biến aDaySalary và workedDay. User tự nhập giá trị
 * Step 2: Tạo hàm function tính lương và button Tính lương
 * Step 3: In kết quả ra console
 *
 * Output: lương nhân viên
 */

function tinhLuong() {
  console.log("yes");

  var aDaySalary = document.getElementById("aDaySalary").value * 1;
  console.log("aDaySalary: ", aDaySalary);

  var workedDay = document.getElementById("workedDay").value * 1;
  console.log(" workedDay: ", workedDay);

  var totalSalary = workedDay * aDaySalary;

  var totalSalary = (document.getElementById("totalSalary").innerHTML =
    totalSalary);
  console.log("totalSalary: ", totalSalary);
}

/**BÀI 2
 *  input: 5 chữ số (num1, num2, num3, num4, num5)
 *
 *
 * Step 1: Tạo biến cho từng chữ số
 * Step 2: Tạo biến cho giá trị trung bình và gắn công thức tính
 * Step 3: Tạo hàm function tính giá trị trung bình và button
 * Step 3: In kết quả ra console
 *
 *
 * Output: Giá trị trung bình
 */

function giaTriTrungBinh() {
  console.log("yes");
  var num1 = document.getElementById("num1").value * 1;

  var num2 = document.getElementById("num2").value * 1;

  var num3 = document.getElementById("num3").value * 1;

  var num4 = document.getElementById("num4").value * 1;

  var num5 = document.getElementById("num5").value * 1;

  var giaTriTrungBinh = (num1 + num2 + num3 + num4 + num5) / 5;
  document.getElementById("result").innerHTML = giaTriTrungBinh;
  console.log("giaTriTrungBinh: ", giaTriTrungBinh);
}

/**BÀI 2
 *
 *
 * Input: số tiền USD cần đổi
 *
 * Step 1: tạo biến cho số tiền USD cần đổi
 * Step 2: tạo hàm function quy đổi tiền và button
 * Step 3: tạo biến quy đổi tiền và gắn công thức tính
 * Step 4: in kết quả ra console
 *
 *
 * Output: số tiền VND
 *
 */
function quyDoiTien() {
  console.log("yes");

  const tienUsEl = 23500;
  var soTienUsEl = document.getElementById("txt-tien-USD").value * 1;

  var result1 = soTienUsEl * tienUsEl;
  document.getElementById("result1").innerHTML = result1;
  console.log("result1: ", result1);
}

/** BÀI 3
 *
 *
 * Input: chu vi hcn và diện tích hcn
 *
 *
 * Step 1: tạo biến cho chiều dài và chiều rộng
 * Step 2: tạo hàm function tính kết quả và button
 * Step 3: tạo biến tính chu vi HCN và diện tích HCN và gắn công thức
 * Step 4: in kết quả ra console
 *
 *
 * Output: kết quả chu vi HCN và diện tích
 *
 */
function ketQua() {
  console.log("yes");

  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;

  var chuVi = (chieuDai + chieuRong) * 2;
  document.getElementById("chuvi").innerHTML = chuVi + ";";
  console.log("chuVi: ", chuVi);

  var dienTich = chieuDai * chieuRong;
  document.getElementById("dientich").innerHTML = dienTich;
  console.log("dienTich: ", dienTich);
}

/**BÀI 4
 *
 *
 * Input: số có 2 chữ số
 *
 * Step 1: tạo biến cho số có 2 chữ số
 * Step 2: tạo biến cho hàng đơn vị và hàng chục theo công thức
 * Step 3: tạo biến tính tổng 2 ký số và gắn công thức
 * Step 4: tạo hàm function tính tổng và button
 * Step 5: in kết quả ra console
 *
 * Output: Tổng của 2 ký số
 *
 */
function tinhTong() {
  var a = document.getElementById("txt-ky-so").value * 1;
  var b = Math.floor(a / 10);
  var c = Math.floor(a % 10);
  var total = b + c;
  document.getElementById("total").innerHTML = total;
  console.log("total: ", total);
}
